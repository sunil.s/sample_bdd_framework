@Loign_Feature
Feature: Login Test

  Background: 
    Given Navigate to Craftsvilla

  Scenario: Login with Invalid credentials
    Given Click on Signup link
    And Login pop up must be displayed
    When Enter "email@gmail.com"
    And also enter "123"
    And Click on Login
    Then Login should be unsuccessful

  Scenario Outline: Login with Invalid credentials
    Given Click on Signup link
    And Login pop up must be displayed
    When Enter "<uname>" and "<pwd>"
    And Click on Login
    Then Login should be unsuccessful

    Examples: 
      | uname           | pwd      |
      | email@gmail.com | asdf1234 |
