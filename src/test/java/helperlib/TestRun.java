package helperlib;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@CucumberOptions(
			features="Feature",
			glue={"stepDef"},
			tags={"@Search_Feature"},
			plugin={"html:target/cucumber-html-report","json:target/cucumber.json","pretty:target/cucumber-pretty.txt","junit:target/cucumber-results.xml"}
		)
public class TestRun extends AbstractTestNGCucumberTests {
	
	@BeforeClass(alwaysRun= true)
	public void setup(){
		System.setProperty("webdriver.chrome.driver","./src/test/resources/chromedriver.exe");
		InitObj.driver= new ChromeDriver();
		InitObj.driver.manage().window().maximize();
		InitObj.driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
	}
	
	@AfterClass(alwaysRun=true)
	public void teardown(){
		InitObj.driver.close();
	}

}
